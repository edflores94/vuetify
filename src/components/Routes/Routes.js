import { createRouter, createWebHistory } from "vue-router";

//declaring components/views
const Home = () => import("../../views/Home");
const About = () => import("../../views/About");

//defining routes
const routes = [
  { path: "/", component: Home },
  { path: "/about", component: About },
];

const router = createRouter({
  history: createWebHistory(),
  routes, // short for `routes: routes`
});

export default router;
