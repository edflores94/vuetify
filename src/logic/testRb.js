function getTestRb() {
  let response = {};
  response.id = 1;
  response.name = "Vue";
  response.description = "Vuetify application";
  response.items = getItems();

  return response;
}

export default getTestRb;

function getItems() {
  const elements = [
    { id: 1, name: "Car toy", price: 15 },
    { id: 2, name: "Guitar", price: 12 }
  ];
  return elements;
}
