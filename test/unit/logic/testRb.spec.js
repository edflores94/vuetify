const testRb = require("logic/testRb");

describe("Prueba test", () => {
  it("Probando que se devuelva items correctos", () => {
    const elements = [
      { id: 1, name: "Car toy", price: 15 },
      { id: 2, name: "Guitar", price: 12 }
    ];
    const getItems = testRb.__get__("getItems");
    const response = getItems();
    
    expect(response).not.toBeNull();
    expect(response).not.toBeUndefined();
    expect(response).toStrictEqual(elements);
  });

  it("Probando que se devuelva requestBody correcto", () => {
    const response = testRb.default();
    expect(response).not.toBeNull();
    expect(response).not.toBeUndefined();
  });
});
